﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareSystem : MonoBehaviour {

    public ParticleSystem[] particles;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EmitParticles() {
        for( var i = 0; i < particles.Length; i ++) {
            particles[i].Emit(10);
        }
    }


}
