﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour {
	private AudioSource _audio;
	public AudioClip[] firingSound;
	public GameObject[] projectiles;
	public Vector3 offset;
	public GameObject ship;

	public float zVelocity;

	int currentIndex = 0;

	void Start() {
		_audio = this.GetComponent<AudioSource>();
	}

	public void SpawnProjectile() {
		Rigidbody rb;
		Vector3 startPos = ship.transform.position + offset;
		Vector3 startRotation = new Vector3(-19f, 0f, ship.transform.rotation.eulerAngles.z);

		//projectiles[currentIndex].SetActive(false);
		currentIndex++;

		if (currentIndex >= projectiles.Length) currentIndex = 0;

		projectiles[currentIndex].transform.position = startPos;
		projectiles[currentIndex].SetActive(true);
		rb = projectiles[currentIndex].GetComponent<Rigidbody>();
		projectiles[currentIndex].transform.eulerAngles = startRotation;

		rb.velocity = new Vector3(0f, 0f, zVelocity);
		_audio.PlayOneShot(firingSound[Random.Range(0, firingSound.Length)]);
	}

	[ContextMenu("Set Projectiles")]
	void Setprojectile() {
		projectiles = GameObject.FindGameObjectsWithTag("Projectile");
	}


}
