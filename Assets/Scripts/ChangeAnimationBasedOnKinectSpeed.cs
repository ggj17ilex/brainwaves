﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ChangeAnimationBasedOnKinectSpeed : MonoBehaviour {
	private TiltPlayerControl _player;
	private Animator _anim;

	public float deltaMaxSpeed = 0.4f;
	
	void Start () {
		if (GameObject.FindGameObjectWithTag("Player") == null) {
			this.enabled = false;
			return;
		}
		this._anim = this.GetComponent<Animator>();
		this._player = GameObject.FindGameObjectWithTag("Player").GetComponent<TiltPlayerControl>();
	}
	
	void Update () {
		if (_player != null) {
			this._anim.speed = 1f + Mathf.Clamp01(_player.totalKinectEnergy / _player.maxKinectEnergy) * deltaMaxSpeed;
		}
	}
}
