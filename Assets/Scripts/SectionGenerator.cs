﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SectionGenerator : MonoBehaviour {
	ObstacleGenerator obstacleGenerator;
	SectionCollection sectionsCollection;
	public float timeToFirstSection = 5f;

	private SectionStructure currentSection;
	private int currentSubsection = 0;

	public Transform[] respawnPoints;

	void Awake () {
		TextAsset sectionText = Resources.Load("sections") as TextAsset;
		sectionsCollection = JsonUtility.FromJson<SectionCollection>(sectionText.text);
		obstacleGenerator = this.GetComponent<ObstacleGenerator>();
		Invoke("loadSection", timeToFirstSection);
	}

	void loadSection() {
		currentSection = sectionsCollection.getRandom();
		if (currentSection == null) {
			Debug.LogError("THERE IS NO SECTION!!!");
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit();
#endif
			return;
		}
		currentSubsection = 0;

		Invoke("summonWave", currentSection.subSections[currentSubsection].distanceFromLast);
	}

	void summonWave() {
		SectionStructure.SubSectionStructure subsection = currentSection.subSections[currentSubsection];
		List<int> positions = new List<int>(subsection.positions);
		//TODO: insert enemy types
		for (int i = 0; i < subsection.amount;i++) {
			int index = Random.Range(0, positions.Count);
			int position = positions[index];
			positions.RemoveAt(index);
			GameObject newObstacle;
			if (subsection.oClass == "ENEMY")
				newObstacle = obstacleGenerator.SpawnObject();
			else
				newObstacle = obstacleGenerator.SpawnSuperObject();
			if (newObstacle == null) {
				Debug.LogError("No more objects of " + subsection.oClass + " type available.");
				return;
			}

			newObstacle.transform.position = respawnPoints[position].position;
		}

		// prepare nextwave
		currentSubsection++;
		// if this was the last wave, load another section
		if (currentSubsection >= currentSection.subSections.Length) {
			loadSection();
			return;
		}
		//otherwise, send next wave.
		Invoke("summonWave", currentSection.subSections[currentSubsection].distanceFromLast);
	}
}
