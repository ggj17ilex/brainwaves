﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour {

	public GameObject[] obstacles;
	public GameObject[] superObjects;

	int initialIndex;
	int currentIndex;

	int superInitialIndex;
	int superIndex;
	public int nObstaclePerTime;
	public int nSuperObstaclesPerTime;

	public Sprite[] asteroidSprites;
	public AudioClip[] superSounds;

	void Start () {
		currentIndex = nObstaclePerTime - 1 ;
		initialIndex = 0;
		//StartCoroutine(GenerateObstacleUntilN());


		superInitialIndex = 0;
		superIndex = nSuperObstaclesPerTime -  1;
	}

	[ContextMenu("Set Obstacle Array")]
	void SetObstacleArray() {
		obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
		for (var i = 0; i < obstacles.Length; i++) {
			obstacles[i].GetComponent<Obstaclebehavior>().SetSpawner(this);
		}
	}

	[ContextMenu("Set Super Obstacle Array")]
	void SetSuperObstacleArray() {
		superObjects = GameObject.FindGameObjectsWithTag("SuperObstacle");
		for (var i = 0; i < superObjects.Length; i++) {
			superObjects[i].GetComponent<Obstaclebehavior>().SetSpawner(this);
		}
	}

	public GameObject SpawnObject() {
		obstacles[initialIndex].SetActive(false);
		initialIndex++;
		currentIndex++;

		if (initialIndex >= obstacles.Length) {
			initialIndex = 0;
		}

		if (currentIndex >= obstacles.Length) {
			currentIndex = 0;
		}

		obstacles[currentIndex].SetActive(true);
		obstacles[currentIndex].GetComponent<Obstaclebehavior>().RespawnObstacle();
		return obstacles[currentIndex];

	}

	public GameObject SpawnSuperObject () {
		superObjects[initialIndex].SetActive(false);
		initialIndex++;
		currentIndex++;

		if (initialIndex >= superObjects.Length) {
			initialIndex = 0;
		}

		if (currentIndex >= superObjects.Length) {
			currentIndex = 0;
		}

		superObjects[currentIndex].SetActive(true);
		superObjects[currentIndex].GetComponent<Obstaclebehavior>().RespawnObstacle();
		return superObjects[currentIndex];

	}

	public IEnumerator GenerateObstacleUntilN() {
		float time = 6.5f / nObstaclePerTime;

		for( var i = 0; i < nObstaclePerTime; i++) {
			yield return new WaitForSeconds(time * (i + 1));
			SpawnObject();
		}     
	}

	public Sprite getAsteroidSprite() {
		int index = Random.Range(0, asteroidSprites.Length);
		return asteroidSprites[index];
	}

	public AudioClip getSuperAudio() {
		return superSounds[Random.Range(0, superSounds.Length)];
	}
}
