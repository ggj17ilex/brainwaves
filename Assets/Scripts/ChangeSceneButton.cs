﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneButton : MonoBehaviour {
	public string destination;

	public void act() {
		SceneManager.LoadScene(destination);
	}
}
