﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstaclebehavior : MonoBehaviour {

	public enum ObstacleType { Regular, Super };

	public ObstacleType obsType;

	public float angleRotation = 20f;
	public float startAngle;

	float zRotation = 5.0F;
	public float speed = 2f;
	public float zSpeed = 0f;
	public float zLimitPos = -7.8f;

	/**********************************************************************
	* Important parameters
	* Z start pos: 40f
	* Y start pos: 1.07f, 0f, 1.07f
	* X start pos: -3.4f, -0.42f, 3.4f
	* startAngle: -30, 0, 30
	**********************************************************************/

	int obstaclePositionType = -1; //0, 1 or 2

	Rigidbody rb;
	public ObstacleGenerator generator;

	public Vector3[] possiblePos;

	public ParticleSystem particleSystem;
	public SpriteRenderer sRend;

	public float maxZVelocity = 80f;
	public float minZVelocity = 30f;
	public float direction;

	public float zSuperVelocity;
	
	void Start() {
		var start_position = Vector3.zero;

		if(obsType == ObstacleType.Regular) {
			sRend.sprite = generator.getAsteroidSprite();
			direction = Mathf.Pow(-1f, Random.Range(1, 3));
		}
		else {
			GetComponent<AudioSource>().clip = generator.getSuperAudio();
		}

		obstaclePositionType = Random.Range(0, possiblePos.Length);
		start_position = possiblePos[obstaclePositionType];

		gameObject.transform.position = start_position;

		//RigidBody Setup
		rb = GetComponent<Rigidbody>();
	}

	void FixedUpdate()
	{
		//zRotation += Input.GetAxis("Horizontal");

		if( obsType == ObstacleType.Regular) {
			sRend.transform.Rotate(Vector3.forward, direction * Random.Range(minZVelocity, maxZVelocity) * Time.deltaTime);
		}
		else {
			sRend.transform.Rotate(Vector3.forward, zSuperVelocity * Time.deltaTime);
		}
		
		//transform.eulerAngles = new Vector3(10, 0, zRotation);

		rb.velocity = new Vector3(0, 0, -zSpeed);

		if (transform.position.z < zLimitPos) {
			//generator.SpawnObject();
			gameObject.SetActive(false);
		}
	}


	void Example()
	{
		print(transform.eulerAngles.x);
		print(transform.eulerAngles.y);
		print(transform.eulerAngles.z);
	}

	[ContextMenu("Set Obstacle Angle")]
	void SetObstacle() {
		print(this.gameObject);
		print(angleRotation);
	}

	public void RespawnObstacle() {
		Start();
	}

	public void SetSpawner(ObstacleGenerator script) {
		generator = script;
	}

	public void Vanish() {
		gameObject.SetActive(false);
		//RespawnObstacle();
	}

	void OnTriggerEnter(Collider other) {
		if(other.tag == "Projectile") {
			if( obsType == ObstacleType.Regular) {
				//Particles and active = false
				other.gameObject.SetActive(false);
				particleSystem.transform.parent = other.transform.parent.parent;

				particleSystem.transform.position = other.transform.position;
				particleSystem.Emit(50);

				particleSystem.GetComponent<AudioSource>().Play();

				gameObject.SetActive(false);
				//RespawnObstacle();
			}
			else {
				other.gameObject.SetActive(false);
				particleSystem.transform.parent = other.transform.parent.parent;

				particleSystem.transform.position = other.transform.position;
				GetComponent<AudioSource>().Play();
				particleSystem.Emit(20);
				particleSystem.gameObject.GetComponent<FlareSystem>().EmitParticles();
			}
		   
		}
	}

	private void OnParticleCollision (GameObject other) {
		Vanish();
	}
}
