﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SectionStructure {
	public enum ObjectClass { ENEMY, INDESTRUCTIBLE, SPECIAL };
	
	[Serializable]
	public class SubSectionStructure {
		public int[] positions;
		public string oClass;
		public int amount;
		public float distanceFromLast;
	}

	public int difficulty; // 0 - VERY EASY; 5 - VERY HARD
	public SubSectionStructure[] subSections;
}

[Serializable]
public class SectionCollection {
	public SectionStructure[] sections;

	public SectionStructure getRandom() {
		if ((sections == null) || (sections.Length == 0)) {
			return null;
		}


        return sections[UnityEngine.Random.Range(0, sections.Length)];
	}
}
