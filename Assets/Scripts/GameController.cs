﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
	public Canvas gameOverCanvas;

	[System.Serializable]
	public struct DamageProperties {
		public string typename;
		public float damagePoints;
	}
	public DamageProperties[] damageList;
	public BarBehavior[] bars;
	public ShipDamageBehavior damageBehavior;
	public TiltPlayerControl tiltControl;

	//Player Properties
	public bool glowTime = false;

	public GameObject camera;


	public void DamageShip(DamageProperties prop) {
		bool dead = bars[0].Damage(prop.damagePoints);

		
		if(dead) {
			GameOver();
			ShakeCamera(3f);
		}
		else {
			ShakeCamera(0.5f);
		}
	}

	public void DecreaseKinectEnergy(DamageProperties prop) {
		bars[1].Damage(prop.damagePoints);
		tiltControl.DecreaseTotalKinectEnergy(prop.damagePoints);

	}

	public void GameOver() {
		damageBehavior.TriggerExplosion();
		gameOverCanvas.gameObject.SetActive(true);
	}

	public void SetBarValue(BarBehavior.BarType barType, float value) {
		if (barType == BarBehavior.BarType.Health) {
			//bars[0].SetValue(value);
		}
		else {
			bool glow = bars[1].SetValue(value);
			if (glow && !glowTime) {
				glowTime = true;
			}
			else if( !glow && glowTime) {
				glowTime = false;
			}
		}
	}

	public void ShakeCamera(float time) {
		iTween.ShakePosition(camera, new Vector3(0.4f, 0.4f, 0f), time);
	}
}
