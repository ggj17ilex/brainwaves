﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinectColorChanger : MonoBehaviour {
	public Color lowEnergy;
	public Color mediumEnergy;
	public Color highEnergy;

	private SpriteRenderer _sprite;
	private TiltPlayerControl _player;

	void Awake () {
		this._sprite = this.GetComponent<SpriteRenderer>();
	}

	private void Start () {
		if (GameObject.FindGameObjectWithTag("Player") == null) {
			this.enabled = false;
			return;
		}
		this._player = GameObject.FindGameObjectWithTag("Player").GetComponent<TiltPlayerControl>();
	}
	
	void FixedUpdate () {
		Color finalColor = lowEnergy;
		float percentage = _player.totalKinectEnergy / _player.maxKinectEnergy;
		percentage *= 3f;
		if (percentage < 1f) {
			finalColor = Color.Lerp(lowEnergy, mediumEnergy, percentage);
		} else if (percentage < 2f) {
			finalColor = Color.Lerp(mediumEnergy, highEnergy, percentage-1f);
		} else {
			finalColor = highEnergy;
		}
		this._sprite.color = finalColor;
	}
}
