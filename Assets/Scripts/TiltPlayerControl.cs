﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class TiltPlayerControl : MonoBehaviour {
	private DistanceJoint2D _joint2D;
	private Rigidbody2D _rigidbody;
	public float maxAngle = 40f;
	public Transform center;
	public float totalKinectEnergy = 0f;
	public float deltaKinectEnergy = 0.03f;
	public float maxKinectEnergy = 100f;
	public float minKinectEnergy = 0f;

    public GameController controller; 

	private void Awake () {
		this._rigidbody = this.GetComponent<Rigidbody2D>();
		this._joint2D = this.GetComponent<DistanceJoint2D>();
	}

	void FixedUpdate () {
#if (!MOBILE_INPUT || UNITY_EDITOR)
		float h = Input.GetAxis("Horizontal");
#else
		float h = CrossPlatformInputManager.GetAxis("Horizontal");
#endif
		//change gravity direction to the "correct" position
		Vector3 g = new Vector3(0f, -1f, 0f);
		Quaternion q = Quaternion.Euler(0f, 0f, h * maxAngle);
		g = q * g;
		Physics2D.gravity = g;

		Quaternion centerRotation = Quaternion.LookRotation(center.position - this.transform.position, -Vector3.forward);
		this.transform.rotation = Quaternion.Euler(0f,0f, centerRotation.z*180f);

		float projecao = Vector2.Dot(g, this._rigidbody.velocity);
		if (projecao > 0.1f) {
			totalKinectEnergy += deltaKinectEnergy;
			if (totalKinectEnergy > maxKinectEnergy * 1.2f)
				totalKinectEnergy = maxKinectEnergy * 1.2f;
		} else {
			totalKinectEnergy -= deltaKinectEnergy * 2f;//Mathf.Max(Mathf.Abs(projecao) * deltaKinectEnergy,deltaKinectEnergy);
			if (totalKinectEnergy < minKinectEnergy)
				totalKinectEnergy = minKinectEnergy;
		}

        controller.SetBarValue(BarBehavior.BarType.Kinect, totalKinectEnergy / 20f );
	}

    public void DecreaseTotalKinectEnergy(float perc) {
        totalKinectEnergy = totalKinectEnergy * (1f - perc);
		this._rigidbody.velocity = this._rigidbody.velocity * (1f - perc);

	}
}
