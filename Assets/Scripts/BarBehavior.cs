﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarBehavior : MonoBehaviour {

    public enum BarType { Health, Kinect };

    public BarType barType;
	public Image image;

    public GameController controller;
	public Sprite[] sprites;
	[SerializeField]
	private float currentValue;

	// Use this for initialization
	void Start () {
        if (barType == BarType.Health) {
			SetValue(1f);
        }
        else {
			SetValue(0f);
        }
	}

    public bool Damage(float damage) {
        bool dead = false;
		SetValue(currentValue - damage);
		//Debug.Log("Vida: " + currentValue);
        if(currentValue <= 0) {
            dead = true;
        }
        return dead;
    }

    public void Restore(float points) {
		SetValue(currentValue + points);
	}

    public bool SetValue(float value) {
		currentValue = value;
		redraw();
        if(currentValue >= 1) {
            return true;
        }
        return false;
    }

	public void redraw() {
		if (sprites.Length > 0) {
			int index = Mathf.FloorToInt(currentValue * sprites.Length);
			if (index >= sprites.Length) {
				index = sprites.Length - 1;
			} else if (index < 0) {
				index = 0;
			}

			this.image.sprite = sprites[index];
		} else {
			this.image.fillAmount = currentValue;
		}
	}
}
