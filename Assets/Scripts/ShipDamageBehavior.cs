﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDamageBehavior : MonoBehaviour {
	public GameController controller;
    public ParticleSystem particle;
    public ParticleSystem shipExplosion;
    public float damagePerCollision;

    void Start() {

    }

    void OnTriggerEnter(Collider other) {
        if ((other.tag == "Obstacle")|| (other.tag == "SuperObstacle")) {
            controller.DamageShip(controller.damageList[0]);
            other.GetComponent<Obstaclebehavior>().Vanish();
        }
        PlayParticles();
    }

    //Play particles 
    void PlayParticles() {
        particle.Emit(50);
    }

    public void TriggerExplosion() {
        shipExplosion.gameObject.transform.parent = transform.parent.parent;
        shipExplosion.gameObject.SetActive(true);
        shipExplosion.Play();
        transform.parent.gameObject.SetActive(false);
    }
}
