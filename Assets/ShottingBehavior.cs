﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShottingBehavior : MonoBehaviour {

	public GameController controller;
	public ProjectileManager projectileManager;
	public ParticleSystem shockwave;
	private float shockwaveCost = 5f;

	// Use this for initialization
	void Start () {
		InvokeRepeating("basicShooting", 2f, 0.3f);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			//controller.DecreaseKinectEnergy(controller.damageList[1]);
		//	projectileManager.SpawnProjectile();
		//} else if (Input.GetButtonDown("Fire2")) {
			if (this.GetComponent<TiltPlayerControl>().totalKinectEnergy > shockwaveCost) {
				shockwave.Play();
				this.GetComponent<TiltPlayerControl>().totalKinectEnergy -= shockwaveCost;
			}
		}
	}

	void basicShooting() {
		projectileManager.SpawnProjectile();
	}
}
